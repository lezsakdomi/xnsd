const xnsd = require('./index');
const dns = require('dns');

if (1 === 2) {
	dns.promises = dns;
}

const d = xnsd();

const zone = d.zone('letlink.me.');
zone.origin = 'letlink.me.';
zone.ttl = 2;
zone.authorative = true;
const iph = zone.zone('iph'); // we declare it here to prevent adding authority records
zone.query((req, res, next) => { // add authority SOA and NS records
	req.d.lookup(req.zone.name, ['SOA', 'NS'])
		.forEach(record => res.authority.add(record));
	res.additional.addNULL(req.zone.resolve('@'),
		Buffer.alloc(57, 'Authority section filled automatically based on zone name'));
	next();
})

zone.SOA('@', 'ns1', 'domonkos\.lezsak.weborigo.eu.', 1807300918, 120, 5, 3600)
	.NS((req, res) => res.addNS('ns1'))
	.NS((req, res) => res.addNS('ns2'));
zone.A('ns1', () => dns.promises.lookup('panel.weborigo.eu'));
zone.A('ns2', () => dns.promises.lookup('static04.stoplossmanager.com'));

zone.A('@', () => dns.promises.lookup('panel.weborigo.eu'));

zone.ANY('www', (req, res) => {
	const target = '@';

	res.answer.addCNAME(target); // res.addCNAME would simplify our job like ns.letlink.me. does
	req.d.lookup(zone.resolve(target), xnsd.ANY)
		.forEach(record => res.additional.add(record));
});

zone.NS('iph', (req, res) => { // `iph.NS('@', 'iph-ns..')` would be enough
	res.answer.addNS('iph-ns');
	res.authorative = false;
	res.authority.clear();
});
zone.A('iph-ns', () => dns.promises.lookup('static06.stoplossmanager.com'));

//const iph = zone.zone('iph');
iph.origin = 'iph';
iph.authorative = false;

iph.A(/^([0-9a-f]{8})$/,
	(req) => [0, 2, 4, 6].map(i => parseInt(req.matches[0].slice(i, i + 2), 16)));