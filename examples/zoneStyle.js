const xnsd = require('./index');
const dns = require('dns');

if (1 === 2) {
	dns.promises = dns;
}

const d = xnsd();

const zone = d.zone('letlink.me.');
zone.origin = 'letlink.me.';
zone.ttl = 2;

zone.addSOA('@', 'ns1', 'domonkos\.lezsak.weborigo.eu.', 1807300918, 120, 5, 3600, ttl)
	.addNS('ns1')
	.addNS('ns2');
zone.addA('ns1', '80.211.226.227'); // panel.weborigo.eu
zone.addA('ns2', '80.211.191.117'); // static04.stoplossmanager.com

zone.addA('@', '80.211.226.227'); // panel.weborigo.eu

zone.addCNAME('www', '@');

zone.addNS('iph', 'iph-ns');
zone.addA('iph-ns', '80.211.191.117'); // static06.stoplossmanager.com