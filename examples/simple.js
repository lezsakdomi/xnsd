const xnsd = require('../index');
const dns = require('dns');

if (1 === 2) {
	dns.promises = dns;
}

const d = xnsd();

const domain = d.addAuthority('letlink.me.', {
	admin: 'domonkos.lezsak@outlook.com',
	master: 'ns1',
	servers: {'ns1': 'panel.weborigo.eu.', 'ns2': 'static04.stoplossmanager.com.'},
});
domain.addHost('@', 'panel.weborigo.eu.');
const iph = domain.delegate('iph', {'iph-ns..': 'static06.stoplossmanager.com.'});
// .. indicates the previous level (letlink.me.)

iph.addHost(/^([0-9a-f]{8})$/,
	(req) => [0, 2, 4, 6].map(i => parseInt(req.matches[0].slice(i, i + 2), 16)));